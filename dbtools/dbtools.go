package dbtools

// cSpell:ignore dbname, dineshappavoo, basex

import (
	"fmt"

	"bitbucket.org/mayowa/helpers/simpleflake"

	"github.com/go-ini/ini"
)

// DbCfg holds the information required for a database connection
type DbCfg struct {
	Name     string
	Driver   string `ini:"driver"`
	DbName   string `ini:"dbname"`
	User     string `ini:"user"`
	Password string `ini:"password"`
	Host     string `ini:"host"`
	Port     int    `ini:"port"`
	Echo     bool   `ini:"-"`
}

// String satisfies the Stringer interface
func (d DbCfg) String() string {
	return d.MakeString(false)
}

/*
MakeString returns a paramater string that can be passed to database/sql.open
@safe: when true, password content = "***"
*/
func (d DbCfg) MakeString(safe bool) string {
	var retv string

	password := d.Password
	if safe {
		password = "***"
	}

	switch d.Driver {
	case "sqlite", "sqlite3":
		retv = d.DbName
	case "pg", "postgres":
		retv = fmt.Sprintf("user=%s dbname=%s", d.User, d.DbName)
		if len(password) > 0 {
			retv = fmt.Sprintf("%s password='%s'", retv, password)
		}
		if len(d.Host) > 0 {
			retv = fmt.Sprintf("%s host=%s", retv, d.Host)
		}
		if d.Port > 0 {
			retv = fmt.Sprintf("%s port=%d", retv, d.Port)
		}

		// ssl off
		retv = fmt.Sprintf("%s sslmode=disable", retv)
	}

	return retv
}

// DSN returns the config as a dsn formated string
func (d DbCfg) DSN() string {
	// drivername://user:possword@host:port/db

	dFmt := "%s://%s:%s@%s/%s"

	host := d.Host
	if len(host) == 0 {
		host = "localhost"
	}

	if d.Port > 0 {
		host = fmt.Sprintf("%s:%d", host, d.Port)
	}

	dNme := ""
	dsn := ""

	switch d.Driver {
	case "pg", "pgx":
		dNme = "postgresql"
		dsn = fmt.Sprintf(dFmt, dNme, d.User, d.Password, host, d.DbName)
	case "sqlite", "sqlite3":
		dNme = "sqlite3"
		dFmt = "%s"
		dsn = fmt.Sprintf(dFmt, d.DbName)
	default:
		dNme = d.Driver
		dsn = fmt.Sprintf(dFmt, dNme, d.User, d.Password, host, d.DbName)
	}

	return dsn
}

/*
GetDbConfig gets the information needed to connect to the database from
config.ini

@section is expected to be the name of a section in the config file
that contains the db connection parameters.alphabet
when @section == "" then section defaults to "db"
*/
func GetDbConfig(section string, cfg *ini.File) (dbCfg *DbCfg, err error) {
	if cfg == nil {
		return nil, fmt.Errorf("cfg parameter cannot be nil")
	}

	dbSection, err := cfg.GetSection(section)
	if err != nil {
		return nil, err
	}

	dbCfg = &DbCfg{}
	err = dbSection.MapTo(dbCfg)
	if err != nil {
		return nil, err
	}

	dbCfg.Name = section
	dbCfg.Echo = true
	switch dbCfg.Driver {
	case "pg", "pgx":
		dbCfg.Driver = "postgresql"

		// check for insufficient params
		if dbCfg.DbName == "" || dbCfg.User == "" || (dbCfg.Host == "" && dbCfg.Port == 0) {
			err = fmt.Errorf("insufficient paramenters: one of dbname, user, host or port not provided")
			return
		}

	case "sqlite":
		dbCfg.Driver = "sqlite3"
		// check for insufficient params
		if dbCfg.DbName == "" {
			err = fmt.Errorf("insufficient paramenters: dbname not provided")
			return
		}

	}

	return
}

// NewID generate a unique id
func NewID() (int64, error) {
	return simpleflake.Generate()
}

// github.com/dineshappavoo/basex
func unquoteIfQuoted(value interface{}) (string, error) {
	bytes, ok := value.([]byte)
	if !ok {
		return "", fmt.Errorf("Could not convert value '%+v' to byte array", value)
	}

	// If the amount is quoted, strip the quotes
	if len(bytes) > 2 && bytes[0] == '"' && bytes[len(bytes)-1] == '"' {
		bytes = bytes[1 : len(bytes)-1]
	}
	return string(bytes), nil
}
