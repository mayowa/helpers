package dbtools

import (
	"encoding/binary"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDBUIDSetByte(t *testing.T) {
	assert := assert.New(t)

	val := int64(28)
	byt := make([]byte, 8)
	binary.BigEndian.PutUint64(byt, uint64(val))

	dval := NewDBUID()
	assert.Nil(dval.Set(byt))

	assert.Equal(val, dval.Get())
}

func TestDBUIDReadByte(t *testing.T) {
	assert := assert.New(t)

	val := int64(28)
	byt := make([]byte, 8)
	binary.BigEndian.PutUint64(byt, uint64(val))

	dval := NewDBUIDInt(28)

	assert.Equal(val, dval.Get())
}
