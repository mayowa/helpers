package validate

import (
	"database/sql/driver"
	"fmt"
	"reflect"

	"strings"

	"github.com/shopspring/decimal"
	"gopkg.in/go-playground/validator.v9"
)

// Validate main validator class
type Validate struct {
	Vtr    *validator.Validate
	Errors map[string]string
}

// NewValidator Creates a new Validate instance
func NewValidator() *Validate {

	v := &Validate{}
	v.Vtr = validator.New()
	v.Vtr.RegisterCustomTypeFunc(ValidateValuer, decimal.Decimal{})

	return v
}

// IsValid ...
func (v *Validate) IsValid(structPtr interface{}) bool {
	v.Errors = map[string]string{}

	//validate
	if err := v.Vtr.Struct(structPtr); err != nil {
		verr, ok := err.(validator.ValidationErrors)
		if !ok {
			return false
		}

		for _, e := range verr {
			p := strings.Split(e.Field(), ".")
			fNme := p[len(p)-1]
			v.Errors[fNme] = MakeErrMessage(e)
		}

		return false
	}

	return true
}

// IsValid convineinece function
func IsValid(structPtr interface{}) *Validate {
	v := NewValidator()
	v.Errors = map[string]string{}

	//validate
	if err := v.Vtr.Struct(structPtr); err != nil {
		verr, ok := err.(validator.ValidationErrors)
		if !ok {
			v.Errors["err"] = err.Error()
			return v
		}

		for _, e := range verr {
			p := strings.Split(e.Field(), ".")
			fNme := strings.ToLower(p[len(p)-1])
			v.Errors[fNme] = MakeErrMessage(e)
		}

		return v
	}

	return nil
}

// MakeErrMessage ...
func MakeErrMessage(erf validator.FieldError) string {
	var msg string
	// fldName := strings.ToLower(errFld.Field())
	tag := erf.Tag()
	param := erf.Param()

	switch tag {
	case "required":
		msg = fmt.Sprintf("Required field")
	case "len", "min":
		msg = fmt.Sprintf("Must have at least %v characters", param)
	case "max":
		msg = fmt.Sprintf("Cannot have more than %v characters", param)
	case "lt":
		msg = fmt.Sprintf("Must be less than %v ", param)
	case "lte":
		msg = fmt.Sprintf("Must be less than or equal to %v ", param)

	case "gt":
		msg = fmt.Sprintf("Must be greater than %v ", param)
	case "gte":
		msg = fmt.Sprintf("Must be greater than or eqaual to %v ", param)

	case "alpha":
		msg = fmt.Sprintf("Must contain only alphabetic chracters %v ", param)
	case "alphanum":
		msg = fmt.Sprintf("Must contain only alphanumeric chracters %v ", param)
	case "number", "numeric":
		msg = fmt.Sprintf("Must contain only numeric chracters %v ", param)

	case "email", "ip", "ipv4", "ipv6", "mac":
		msg = fmt.Sprintf("Not a valid %s address ", tag)
	case "url", "uri":
		msg = fmt.Sprintf("Not a valid %s ", tag)
	case "unqstr":
		msg = fmt.Sprintf("%s must be unique", erf.Field())
	default:
		msg = fmt.Sprintf("Validation error!")
	}

	return msg
}

// ValidateValuer implements validator.CustomTypeFunc
func ValidateValuer(field reflect.Value) interface{} {
	if valuer, ok := field.Interface().(driver.Valuer); ok {
		val, err := valuer.Value()
		if err == nil {
			return val
		}
		// handle the error how you want
		fmt.Println("ValidateValuer:", err)
	}
	return nil
}
