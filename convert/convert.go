package convert

// cSpell:ignore Atoui, Atoui64

import (
	"strconv"
	"time"
)

// YearDiff calculates the difference between tow time.Time and returns the
// difference in terms of Years
func YearDiff(first, second time.Time) int {
	Year := time.Duration(time.Hour * 24 * 365)
	diff := second.Sub(first)
	yd := int(diff / Year)
	return yd
}

// Atoi Converts a string to an int, returns zero if string is empty or contains
// an invalid int
func Atoi(val string) int {
	i, err := strconv.Atoi(val)
	if err != nil {
		return 0
	}

	return i
}

// Atoi64 Converts a string to an int64, returns zero if string is empty or contains
// an invalid int64
func Atoi64(val string) int64 {
	i, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		return int64(0)
	}

	return i
}

// Atoui64 Converts a string to uint64, returns zero if string is empty or contains
// an invalid uint64
func Atoui64(val string) uint64 {
	i, err := strconv.ParseUint(val, 10, 64)
	if err != nil {
		return uint64(0)
	}

	return i
}
