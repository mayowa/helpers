package config

// cSpell:ignore mitchellh

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	lPath "bitbucket.org/mayowa/helpers/path"

	"github.com/go-ini/ini"
	"github.com/mitchellh/go-homedir"
)

const cfgFileName = "config.ini"

// Load returns a pointer to ini.File which represents
// the applications config.ini
func Load(appName string) (cfg *ini.File, cfgPath string, err error) {
	cfgPath, err = GetPath(appName)
	if err != nil {
		return
	}

	cfgFile := filepath.Join(cfgPath, cfgFileName)
	if cfg, err = ini.Load(cfgFile); err != nil {
		return
	}

	return
}

// Create create config based on provided content
// if file exists, it is loaded and an error is returned as a warning
func Create(appName, content string) (cfg *ini.File, cfgPath string, err error) {
	pthFound, fleFound, err := cfgFileExists(appName)
	if err != nil {
		return
	}

	// config folder doesn't exist: create
	cfgPath, err = GetPath(appName)
	if !pthFound {
		if err != nil {
			return
		}

		err = os.Mkdir(cfgPath, 0700)
		if err != nil {
			return
		}
	}

	// config.ini exist, load and return it with an error message
	if fleFound {
		cfg, cfgPath, err = Load(appName)
		if err == nil {
			err = fmt.Errorf("Warning: config file [%s] already exists", cfgPath)
		}

		return
	}

	// config.ini doesn't exist, create it
	txt := bytes.NewBufferString(content)

	cfg, err = ini.Load(txt.Bytes())
	if err != nil {
		return
	}

	cfgFile := filepath.Join(cfgPath, cfgFileName)
	err = cfg.SaveTo(cfgFile)
	if err != nil {
		return
	}

	return
}

// GetPath returns path where config.ini is located
// Note that GetPath will first check in the current directory for
// .config, if it doesn't find it, it then defaults the the os suggested
// config locations
func GetPath(appName string) (string, error) {
	var err error
	var pth string

	// check for ".config" in current directory
	cwd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	pth = filepath.Join(cwd, ".config")
	if lPath.Available(pth) {
		return pth, nil
	}

	// check for os specific config locations
	if runtime.GOOS == "windows" {
		pth, err = getWinConfigPath()
		if err != nil {
			return "", err
		}
	} else {
		pth, err = getNixConfigpath()
		if err != nil {
			return "", err
		}
	}

	pth = filepath.Join(pth, appName)
	return pth, nil
}

// FileExists checks to see if the config file exists
// first return value indicates if the path exists
// second return value indicates if the file exists within the path
// doesn't return an error if pathFound or fileFound == false
func cfgFileExists(appName string) (pathFound, fileFound bool, err error) {
	// get config file path
	cfgPath, err := GetPath(appName)
	if err != nil {
		return
	}

	// check if path exists
	_, err = os.Stat(cfgPath)
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		}

		return
	}

	// path exists
	pathFound = true

	// check if file exists
	cfName := filepath.Join(cfgPath, cfgFileName)
	_, err = os.Stat(cfName)
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		}

		return
	}

	// file exists
	fileFound = true

	return
}

func getWinConfigPath() (string, error) {
	path := os.Getenv("APPDATA")
	return path, nil
}

func getNixConfigpath() (string, error) {
	homepath, err := homedir.Dir()
	if err != nil {
		return "", nil
	}

	path := filepath.Join(homepath, ".config")
	return path, nil
}
