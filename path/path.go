/*
Package lib : functions in tools.go must be self contained or rely only on other functions
found in this file.
This because package lib is an incubator and "sub packages" that become mature will
eventually be moved into their own top-level package
*/
package path

import (
	"bytes"
	"io"
	"os"
)

// Exists determines if a path exists
func Exists(path string) (bool, error) {

	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			err = nil
		}

		return false, err
	}

	return true, nil
}

// Available determines if a path exists on error returns false
func Available(path string) bool {
	retv, _ := Exists(path)
	return retv
}

// SaveTo saves from io.Reader to a file
func SaveTo(file string, r io.Reader) error {
	fh, err := os.Create(file)
	if err != nil {
		return err
	}
	defer fh.Close()

	var buf bytes.Buffer
	if _, err = buf.ReadFrom(r); err != nil {
		return err
	}

	if _, err := buf.WriteTo(fh); err != nil {
		return err
	}

	return nil
}

// SaveStrTo saves from io.Reader to a file
func SaveStrTo(file string, buff string) error {
	return SaveTo(file, bytes.NewBufferString(buff))
}

// SaveBytTo saves from []byte to a file
func SaveBytTo(file string, buff []byte) error {
	return SaveTo(file, bytes.NewBuffer(buff))
}
